const express = require("express");
const app = express();
app.use(express.json());
const routes=require("./routes/api");
routes(app);


initializeServer = () => {
 try{
    app.listen(3000, () => {
      console.log("Server Running at http://localhost:3000/");
    });
  } catch (error) {
    response.status(500)
    response.send({error:error.message});
    process.exit(1);
  }
};
initializeServer();

