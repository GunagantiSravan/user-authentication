const jwt=require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { usernameValidation, emailValidation, passwordValidation }=require("./validation");
const authentication =require('./authentication');
let usersData=[]

module.exports = function(app){
  try{
      app.post("/signup/", async (request, response) => {
      const {username,email,password} = request.body;
      const foundUser = usersData.find(user=>user.username===username);
        if(!usernameValidation(username)&& username!==""){
          response.status(400);
          response.send({message:"username is not valid. Only characters A-Z, a-z and '-' are  acceptable."})
        }else if(!emailValidation(email)){
          response.status(400);
          response.send({message:"enter a valid email"});

        }else if (!passwordValidation(password)) {
          response.status(400);
          response.send({message:"Password should contain min 6 characters,1-uppercase ,1-lowercase, 1-specialcharacter,1-number"});
        }else{
          if (foundUser!== undefined){
            response.status(400);
            response.send({message:"User already exists"});
          } else {
              const hashedPassword = await bcrypt.hash(password, 10);
              const createUser = {username,email,password:hashedPassword}
              usersData.push(createUser)
              response.status(200);
              response.send({message:"User created successfully"});
          }
      }
    });
  }catch(error){
    response.status(500);
    response.send({"Error":error.message});
  }


  try{
      app.post("/signin/", async (request, response) => {
        const { username, password } = request.body;
        const foundUser = usersData.find(user=>user.username===username);
        if (foundUser!==undefined) {
          const isPasswordCorrect = await bcrypt.compare(
            password,
            foundUser.password
          );
      
          if (isPasswordCorrect) {
            const payload = { username, email:foundUser.email};
            const jwtToken = jwt.sign(payload, "SECRET_KEY");
            response.status(200);
            response.send({ jwtToken });
          } else {
            response.status(400);
            response.send({message:"Invalid password"});
          }
        } else {
          response.status(400);
          response.send({message:"Invalid user"});
        }
      });
  }catch(error){
    response.status(500);
    response.send({"Error":error.message});
  }
  


  try{
  app.patch("/reset-password",authentication, async (request, response) => {
      const {oldpassword, newpassword } = request.body;
      const{username}=request;
      const foundUser=usersData.find(user=>user.username===username);
      if (foundUser === undefined) {
          response.status(400);
          response.send({message:"Invalid user"});
      } else {
          const isPasswordMatched = await bcrypt.compare(
              oldpassword,
              foundUser.password
            );
          if (isPasswordMatched === true) {
              if (passwordValidation(newpassword)) {
                  const hashedPassword = await bcrypt.hash(newpassword, 10);
                  usersData=usersData.map((user)=>{
                      if(username===user.username){
                          return {username,email:user.email,password:hashedPassword}
                      }else{
                          return user;
                      }
                  });
                  response.status(200);
                  response.send({message:"Password updated successfully"});
              } else {
                response.status(400);
                response.send({message:"Password should contain min 8 characters,uppercase ,lowercase, specialcharacter,number"});
              }   
              
          } else {
              response.status(400);
              response.send({message:"Invalid current password"});
          }
      }
    });
  }catch(error){
    response.status(500);
    response.send({"Error":error.message});
  }

}